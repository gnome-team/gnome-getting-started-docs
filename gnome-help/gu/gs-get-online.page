<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-get-online" xml:lang="gu">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>જેકબ સ્ટેઇનર</name>
    </credit>
    <credit type="author">
      <name>પેટર કોવર</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">ઓનલાઇન થાવ</title>
    <link type="seealso" xref="net"/>
    <title role="seealso" type="link">ઓનલાઇન થવા પર માર્ગદર્શિકા</title>
    <link type="next" xref="gs-browse-web"/>
  </info>

  <title>ઓનલાઇન થાવ</title>
  
  <note style="important">
      <p>You can see the status of your network connection on the right-hand
       side of the top bar.</p>
  </note>  

  <section id="going-online-wired">

    <title>વાયરવાળા નેટવર્ક સાથે જોડાવો</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online1.svg" width="100%"/>

    <steps>
      <item><p>The network connection icon on the right-hand side of the top
       bar shows that you are offline.</p>
      <p>The offline status can be caused by a number of reasons: for
       example, a network cable has been unplugged, the computer has been set
       to run in <em>airplane mode</em>, or there are no available Wi-Fi
       networks in your area.</p>
      <p>જો તમે વાયરવાળુ જોડાણને વાપરવા માંગો છો તો, ઓનલાઇન થવા માટે નેટવર્ક કેબલમાં પ્લગ કરો. કમ્પ્યૂટર તમારી માટે આપમેળે નેટવર્ક જોડાણને સુયોજિત કરવા માટે પ્રયત્ન કરશે.</p>
      <p>જ્યારે કમ્પ્યૂટર તમારી માટે નેટવર્ક જોડાણને સુયોજિત કરે તો, નેટવર્ક જોડાણ ચિહ્ન ત્રણ બિંદુઓને બતાવે છે.</p></item>
      <item><p>એકવાર નેટવર્ક જોડાણ સફળતાપૂર્વક સુયોજિત કરી દેવામાં આવે તો, નેટવર્ક જોડાણ ચિહ્ન નેટવર્ક થયેલ કમ્પ્યૂટર સંકેતમાં બદલાય છે.</p></item>
    </steps>
    
  </section>

  <section id="going-online-wifi">

    <title>Wi-Fi નેટવર્ક સાથે જોડાવો</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online2.svg" width="100%"/>
    
    <steps>
      <title>To connect to a Wi-Fi (wireless) network:</title>
      <item>
        <p>Click the <gui xref="shell-introduction#yourname">system menu</gui>
        on the right side of the top bar.</p>
      </item>
      <item>
        <p>Select <gui>Wi-Fi Not Connected</gui>. The Wi-Fi section of the menu
        will expand.</p>
      </item>
      <item>
        <p>Click <gui>Select Network</gui>.</p>
      </item>
    </steps>

     <note style="important">
       <p>You can only connect to a Wi-Fi network if your computer hardware
        supports it and you are in an area with Wi-Fi coverage.</p>
     </note>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online3.svg" width="100%"/>

    <steps style="continues">
    <item><p>From the list of available Wi-Fi networks, select the network you
    want to connect to, and click <gui>Connect</gui> to confirm.</p>
    <p>Depending on the network configuration, you may be prompted for network
     credentials.</p></item>
    </steps>

  </section>

</page>
