<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-get-online" xml:lang="pa">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>ਜੈਕਬ ਸਟਿਈਨੇਰ</name>
    </credit>
    <credit type="author">
      <name>ਪੇਟਰ ਕੋਵਰ</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">ਆਨਲਾਈਨ ਜਾਉ</title>
    <link type="seealso" xref="net"/>
    <title role="seealso" type="link">ਆਨਲਾਈਨ ਹੋਣ ਲਈ ਸਿਖਲਾਈ</title>
    <link type="next" xref="gs-browse-web"/>
  </info>

  <title>ਆਨਲਾਈਨ ਜਾਉ</title>
  
  <note style="important">
      <p>ਤੁਸੀਂ ਉੱਤੇ ਪੱਟੀ ਵਿੱਚ ਸੱਜੇ ਪਾਸੇ ਆਪਣਾ ਨੈੱਟਵਰਕ ਕੁਨੈਕਸ਼ਨ ਦੀ ਸਥਿਤੀ ਵੇਖ ਸਕਦੇ ਹੋ।</p>
  </note>  

  <section id="going-online-wired">

    <title>ਤਾਰ ਵਾਲੇ ਨੈੱਟਵਰਕ ਨਾਲ ਕੁਨੈਕਟ ਕਰਨਾ</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online1.svg" width="100%"/>

    <steps>
      <item><p>The network connection icon on the right-hand side of the top
       bar shows that you are offline.</p>
      <p>The offline status can be caused by a number of reasons: for
       example, a network cable has been unplugged, the computer has been set
       to run in <em>airplane mode</em>, or there are no available Wi-Fi
       networks in your area.</p>
      <p>ਜੇ ਤੁਸੀਂ ਤਾਰ ਵਾਲਾ ਕੁਨੈਕਸ਼ਨ ਚਾਹੁੰਦੇ ਹੋ ਤਾਂ ਆਨਲਾਈਨ ਜਾਣ ਲਈ ਕੇਵਲ ਨੈੱਟਵਰਕ ਕੇਬਲ ਲਗਾ ਦਿਉ। ਕੰਪਿਊਟਰ ਤੁਹਾਡੇ ਲਈ ਆਪਣੇ-ਆਪ ਨੈੱਟਵਰਕ ਕੁਨੈਕਸ਼ਨ ਸੈੱਟ ਕਰਨ ਦੀ ਕੋਸ਼ਿਸ਼ ਕਰੇਗਾ।</p>
      <p>ਜਦੋਂ ਤੱਕ ਕੰਪਿਊਟਰ ਨੂੰ ਤੁਹਾਡੇ ਲਈ ਨੈੱਟਵਰਕ ਨਾਲ ਸੈਟਅੱਪ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੁੰਦਾ ਹੈ, ਨੈੱਟਵਰਕ ਕੁਨੈਕਸ਼ਨ ਆਈਕਾਨ ਨੂੰ ਤਿੰਨ ਬਿੰਦੂਆਂ ਵਜੋਂ ਵੇਖਾਇਆ ਜਾਂਦਾ ਹੈ।</p></item>
      <item><p>ਜਦੋਂ ਨੈੱਟਵਰਕ ਕੁਨੈਕਸ਼ਨ ਠੀਕ ਤਰ੍ਹਾਂ ਸੈੱਟ ਅੱਪ ਹੋ ਗਿਆ ਤਾਂ ਨੈੱਟਵਰਕ ਕੁਨੈਕਸ਼ਨ ਆਈਕਾਨ ਨੂੰ ਨੈੱਟਵਰਕ ਨਾਲ ਜੁੜੇ ਹੋਏ ਕੰਪਿਊਟਰ ਨਿਸ਼ਾਨ ਨਾਲ ਬਦਲ ਦੇਵੇਗਾ।</p></item>
    </steps>
    
  </section>

  <section id="going-online-wifi">

    <title>ਵਾਈ-ਫਾਈ ਨੈੱਟਵਰਕ ਨਾਲ ਕੁਨੈਕਟ ਕਰਨਾ</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online2.svg" width="100%"/>
    
    <steps>
      <title>To connect to a Wi-Fi (wireless) network:</title>
      <item>
        <p>Click the <gui xref="shell-introduction#yourname">system menu</gui>
        on the right side of the top bar.</p>
      </item>
      <item>
        <p>Select <gui>Wi-Fi Not Connected</gui>. The Wi-Fi section of the menu
        will expand.</p>
      </item>
      <item>
        <p>Click <gui>Select Network</gui>.</p>
      </item>
    </steps>

     <note style="important">
       <p>ਵਾਈ-ਫਾਈ ਨੈੱਟਵਰਕ ਨਾਲ ਤੁਸੀਂ ਕੇਵਲ ਤਾਂ ਹੀ ਕੁਨੈਕਟ ਕਰ ਸਕਦੇ ਹੋ, ਜੇ ਤੁਹਾਡੇ ਕੰਪਿਊਟਰ ਦਾ ਹਾਰਡਵੇਅਰ ਸਹਾਇਕ ਹੈ ਅਤੇ ਤੁਸੀਂ ਵਾਈ-ਫਾਈ ਸੇਵਾ ਦੇ ਖੇਤਰ ਵਿੱਚ ਹੋ।</p>
     </note>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online3.svg" width="100%"/>

    <steps style="continues">
    <item><p>ਮੌਜੂਦ ਵਾਈ-ਫਾਈ ਨੈੱਟਵਰਕ ਦੀ ਸੂਚੀ ਤੋਂ ਜਿਸ ਨੈੱਟਵਰਕ ਨਾਲ ਤੁਸੀਂ ਕੁਨੈਕਟ ਕਰਨਾ ਚਾਹੁੰਦੇ ਹੋ, ਨੂੰ ਚੁਣੋ ਅਤੇ ਤਸਦੀਕ ਕਰਨ ਲਈ <gui>ਕੁਨੈਕਟ ਕਰੋ</gui> ਉੱਤੇ ਕਲਿੱਕ ਕਰੋ।</p>
    <p>ਨੈੱਟਵਰਕ ਸੰਰਚਨਾ ਦੇ ਮੁਤਾਬਕ ਤੁਹਾਨੂੰ ਨੈੱਟਵਰਕ ਸਨਦ ਲਈ ਪੁੱਛਿਆ ਜਾ ਸਕਦਾ ਹੈ।</p></item>
    </steps>

  </section>

</page>
