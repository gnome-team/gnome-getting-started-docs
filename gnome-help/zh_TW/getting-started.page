<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="getting-started" version="1.0 if/1.0" xml:lang="zh-TW">

<info>
    <link type="guide" xref="index" group="gs"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <desc>剛用 GNOME 不久？就來瞭解如何上手。</desc>
    <title type="link">Getting started with GNOME</title>
    <title type="text">入門</title>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Cheng-Chia Tseng</mal:name>
      <mal:email>pswo10680@gmail.com</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

<title>入門</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg">
      <ui:caption its:translate="yes"><desc style="center">啟動應用程式</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>啟動應用程式</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>將您的滑鼠指標移向螢幕左上角的 <gui>概覽</gui>。</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p>點按 <gui>顯示應用程式</gui> 圖示。</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>點按您想要執行的應用程式，舉例來說：「求助」。</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>您也可以改用鍵盤開啟 <gui>活動概覽</gui>，按下 <key href="help:gnome-help/keyboard-key-super">Super</key> 鍵即可。</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>開始輸入您想要啟動的應用程式名稱。</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>按下 <key>Enter</key> 鍵啟動應用程式。</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

</if:when>
</if:choose>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg">
        <ui:caption its:translate="yes"><desc style="center">切換工作</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>切換工作</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">將您的滑鼠指標移向螢幕左上角的 <gui>概覽</gui>。</tt:p>
         </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>點按視窗可以切換至該工作。</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>當螢幕的一半反白時，放開視窗。</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>To maximize a window along the right side, grab the window’s
            titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>當螢幕的一半反白時，放開視窗。</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p>按下 <keyseq> <key href="help:gnome-help/keyboard-key-super">Super</key><key> Tab</key></keyseq> 以顯示 <gui>視窗切換器</gui>。</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>放開 <key href="help:gnome-help/keyboard-key-super">Super </key> 鍵可選取下個反白的視窗。</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>若要在開啟的視窗清單間循環切換，請不要放開 <key href="help:gnome-help/keyboard-key-super">Super</key> 鍵而是按住不放，再按下 <key>Tab</key> 鍵。</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p>按下 <key href="help:gnome-help/keyboard-key-super">Super </key> 鍵會顯示 <gui>活動概覽</gui> 畫面。</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>開始輸入您想切換的應用程式名稱。</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>當應用程式出現在第一個結果之時，按下 <key> Enter</key> 鍵可切換至該程式。</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg">
          <ui:caption its:translate="yes"><desc style="center">使用視窗與工作區</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>視窗與工作區</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>To maximize a window, grab the window’s titlebar and drag it to
            the top of the screen.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>當螢幕反白時，放開視窗。</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>To unmaximize a window, grab the window’s titlebar and drag it
            away from the edges of the screen.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>您也可以點按住頂端列不放，將視窗拖離開來就能取消最大化。</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>當螢幕的一半反白時，放開視窗。</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>To maximize a window along the right side of the screen, grab
            the window’s titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>當螢幕的一半反白時，放開視窗。</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>若要使用鍵盤讓視窗放到最大，請按住 <key href="help:gnome-help/keyboard-key-super">Super</key> 鍵，再按下 <key>↑</key> 鍵。</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>若要讓視窗還原回尚未放到最大的狀態，請按住 <key href="help:gnome-help/keyboard-key-super">Super</key> 鍵，再按下 <key>↓</key> 鍵。</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>若要讓視窗靠螢幕右側放到最大，請按住 <key href="help:gnome-help/keyboard-key-super">Super</key> 鍵，再按下 <key>→</key> 鍵。</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>若要讓視窗靠螢幕左側放到最大，請按住 <key href="help:gnome-help/keyboard-key-super">Super</key> 鍵，再按下 <key>←</key> 鍵。</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>若要將視窗移動到目前工作區之下的工作區，請按 <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Down</key></keyseq> 組合鍵。</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>若要將視窗移動到目前工作區之上的工作區，請按 <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Up</key></keyseq> 組合鍵。</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

<links type="topic" style="grid" groups="tasks">
<title style="heading">Common tasks</title>
</links>

<links type="guide" style="heading nodesc">
<title/>
</links>

</page>
