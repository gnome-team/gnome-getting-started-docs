<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-system-search" xml:lang="zh-TW">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <credit type="author">
      <name>Hannie Dumoleyn</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">使用系統搜尋</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">使用系統搜尋的教學。</title>
    <link type="next" xref="gs-get-online"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Cheng-Chia Tseng</mal:name>
      <mal:email>pswo10680@gmail.com</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>使用系統搜尋</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-search1.svg" width="100%"/>
    
    <steps>
    <item><p>Open the <gui>Activities</gui> overview by clicking <gui>Activities</gui> 
    at the top left of the screen, or by pressing the
    <key href="help:gnome-help/keyboard-key-super">Super</key> key. 
    Start typing to search.</p>
    <p>符合您輸入內容的結果會隨著輸入即時出現。第一個結果總是會反白，並顯示在頂端。</p>
    <p>按下 <key>Enter</key> 鍵切換到第一個反白結果。</p></item>
    </steps>
    
    <media its:translate="no" type="image" mime="image/svg" src="gs-search2.svg" width="100%"/>
    <steps style="continues">
      <item><p>可能出現在搜尋結果中的項目包括：</p>
      <list>
        <item><p>符合的應用程式，會顯示在搜尋結果的頂端、</p></item>
        <item><p>符合的設定值、</p></item>
        <item><p>matching contacts,</p></item>
        <item><p>matching documents,</p></item>
        <item><p>matching calendar,</p></item>
        <item><p>matching calculator,</p></item>
        <item><p>matching software,</p></item>
        <item><p>matching files,</p></item>
        <item><p>matching terminal,</p></item>
        <item><p>matching passwords and keys.</p></item>
      </list>
      </item>
      <item><p>在搜尋結果中，點按項目可以切換至該項目。</p>
      <p>另外，可以使用方向鍵來反白項目，再按下 <key>Enter</key> 鍵切換過去。</p></item>
    </steps>

    <section id="use-search-inside-applications">
    
      <title>從應用程式內部搜尋</title>
      
      <p>系統搜尋彙整許多應用程式的結果。在搜尋結果的左側，您可以看見提供該搜尋結果的應用程式圖示。點按其中之一的圖示，會在與該圖示相連的程式之內重新搜尋。因為 <gui>活動概覽</gui> 畫面內只會顯示最相符的結果，改從應用程式內部搜尋可能會得到更貼切的結果。</p>

    </section>

    <section id="use-search-customize">

      <title>自訂搜尋結果</title>

      <media its:translate="no" type="image" mime="image/svg" src="gs-search-settings.svg" width="100%"/>

      <note style="important">
      <p>Your computer lets you customize what you want to display in the search
       results in the <gui>Activities Overview</gui>. For example, you can
        choose whether you want to show results for websites, photos, or music.
        </p>
      </note>


      <steps>
        <title>若要自訂搜尋結果中要顯示的項目：</title>
        <item><p>Click the <gui xref="shell-introduction#yourname">system menu</gui>
        on the right side of the top bar.</p></item>
        <item><p>Click <gui>Settings</gui>.</p></item>
        <item><p>Click <gui>Search</gui> in the left panel.</p></item>
        <item><p>In the list of search locations, click the switch next to the
        search location you want to enable or disable.</p></item>
      </steps>

    </section>

</page>
