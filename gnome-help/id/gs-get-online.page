<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-get-online" xml:lang="id">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">Menuju daring</title>
    <link type="seealso" xref="net"/>
    <title role="seealso" type="link">Tutorial cara daring</title>
    <link type="next" xref="gs-browse-web"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2013, 2016, 2017, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kukuh Syafaat</mal:name>
      <mal:email>syafaatkukuh@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>Menuju daring</title>
  
  <note style="important">
      <p>Anda dapat melihat status koneksi jaringan Anda di sisi kanan bilah puncak.</p>
  </note>  

  <section id="going-online-wired">

    <title>Menyambung ke jaringan kabel</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online1.svg" width="100%"/>

    <steps>
      <item><p>Ikon koneksi jaringan di sisi kanan bilah puncak menunjukkan bahwa Anda luring.</p>
      <p>Status luring bisa disebabkan oleh sejumlah alasan: sebagai contoh, suatu kabel jaringan telah terlepas, komputer telah ditata agar berjalan dalam <em>mode pesawat terbang</em>, atau tidak ada jaringan Wi-Fi yang tersedia di area Anda.</p>
      <p>Bila Anda ingin memakai jaringa lewat kabel, tancapkan saja kabel jaringan untuk menuju daring. Komputer akan mencoba menyiapkan koneksi jaringan untuk Anda secara otomatis.</p>
      <p>Ketika komputer menyiapkan suatu koneksi jaringan untuk Anda, ikon koneksi jaringan menampilkan tiga titik.</p></item>
      <item><p>Setelah koneksi jaringan telah sukses disiapkan, ikon koneksi jaringan berubah menjadi simbol komputer terjejaring.</p></item>
    </steps>
    
  </section>

  <section id="going-online-wifi">

    <title>Menyambung ke jaringan Wi-Fi</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online2.svg" width="100%"/>
    
    <steps>
      <title>Untuk menyambung ke jaringan Wi-Fi (nirkabel):</title>
      <item>
        <p>Klik <gui xref="shell-introduction#yourname">menu sistem</gui> pada sisi kanan bilah puncak.</p>
      </item>
      <item>
        <p>Pilih <gui>Wi-Fi Tidak Tersambung</gui>. Seksi Wi-Fi dari menu akan mengembang.</p>
      </item>
      <item>
        <p>Klik <gui>Pilih Jaringan</gui>.</p>
      </item>
    </steps>

     <note style="important">
       <p>Anda hanya dapat menyambung ke suatu jaringan Wi-Fi bila perangkat keras komputer Anda mendukungnya dan Anda berada di suatu area dengan cakupan Wi-Fi.</p>
     </note>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online3.svg" width="100%"/>

    <steps style="continues">
    <item><p>Dari daftar jaringan Wi-Fi yang tersedia, pilih jaringan tempat Anda ingin menyambung, dan klik <gui>Sambung</gui> untuk mengkonfirmasi.</p>
    <p>Bergantung kepada konfigurasi jaringan, Anda mungkin dimintai kredensial jaringan.</p></item>
    </steps>

  </section>

</page>
