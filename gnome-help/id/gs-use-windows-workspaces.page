<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-windows-workspaces" xml:lang="id">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">Memakai jendela dan area kerja</title>
    <link type="seealso" xref="shell-windows-switching"/>
    <title role="seealso" type="link">Tutorial tentang memakai jendela dan ruang kerja</title>
    <link type="next" xref="gs-use-system-search"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2013, 2016, 2017, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kukuh Syafaat</mal:name>
      <mal:email>syafaatkukuh@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>Memakai jendela dan area kerja</title>

  <ui:overlay width="812" height="452">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Jendela dan Area Kerja</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>Untuk memaksimalkan suatu jendela, tangkap bilah judul jendela dan seretlah ke puncak layar.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>Ketika layar disorot, lepaskan jendela.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>Untuk tak memaksimalkan suatu jendela, tangkap bilah judul jendela dan seret menjauh dari tepi layar.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>Anda juga dapat mengklik pada bilah puncak untuk menyeret pergi jendela dan tak memaksimalkannya.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>Untuk memaksimalkan suatu jendela sepanjang sisi kiri layar, tangkap bilah judul jendela dan seretlah ke sisi kiri.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>Ketika separuh layar sedang disorot, jatuhkan jendela.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>Untuk memaksimalkan suatu jendela sepanjang sisi kanan layar, tangkap bilah judul jendela dan seretlah ke sisi kanan.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>Ketika separuh layar sedang disorot, jatuhkan jendela.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>Untuk memaksimalkan suatu jendela memakai papan tik, tahan tombol <key href="help:gnome-help/keyboard-key-super">Super</key> dan tekan <key>↑</key>.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>Untuk mengembalikan jendela ke ukuran semula yang tidak termaksimalkan, tahan tombol <key href="help:gnome-help/keyboard-key-super">Super</key> dan tekan <key>↓</key>.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>Untuk memaksimalkan suatu jendela sepanjang sisi kanan layar, tahan tombol <key href="help:gnome-help/keyboard-key-super">Super</key> dan tekan <key>→</key>.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>Untuk memaksimalkan suatu jendela sepanjang sisi kiri layar, tahan tombol <key href="help:gnome-help/keyboard-key-super">Super</key> dan tekan <key>←</key>.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>Untuk berpindah ke suatu ruang kerja yang berada di bawah ruang kerja saat ini, tekan tombol <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Down</key></keyseq>.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>Untuk berpindah ke suatu ruang kerja yang berada di atas ruang kerja saat ini, tekan tombol <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Up</key></keyseq>.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>
  
  <section id="use-workspaces-and-windows-maximize">
    <title>Maksimalkan dan tak maksimalkan jendela</title>
    <p/>
    
    <steps>
      <item><p>Untuk memaksimalkan suatu jendela sehingga itu memakai seluruh ruang pada desktop Anda, tangkap bilah judul jendela dan seretlah ke puncak layar.</p></item>
      <item><p>Ketika layar sedang disorot, jatuhkan jendela untuk memaksimalkannya.</p></item>
      <item><p>Untuk mengembalikan suatu jendela ke ukuran tak maksimal, tangkap bilah judul jendela dan seretlah menjauh dari tepi layar.</p></item>
    </steps>
    
  </section>

  <section id="use-workspaces-and-windows-tile">
    <title>Menjajarkan jendela</title>
    <p/>
    
    <steps>
      <item><p>Untuk memaksimalkan suatu jendela sepanjang sisi layar, tangkap bilah judul jendela dan seretlah ke sisi kiri atau kanan layar.</p></item>
      <item><p>Ketika separuh layar sedang disorot, jatuhkan jendela untuk memaksimalkannya sepanjang sisi layar yang dipilih.</p></item>
      <item><p>Untuk memaksimalkan dua jendela bersisian, tangkap bilah judul dari jendela kedua dan seretlah ke sisi berlawanan dari layar.</p></item>
       <item><p>Ketika separuh layar sedang disorot, jatuhkan jendela untuk memaksimalkannya sepanjang sisi berlawanan dari layar.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-maximize-keyboard">
    <title>Memaksimalkan dan tak memaksimalkan jendela memakai papan tik</title>
    
    <steps>
      <item><p>Untuk memaksimalkan suatu jendela memakai papan tik, tahan tombol <key href="help:gnome-help/keyboard-key-super">Super</key> dan tekan <key>↑</key>.</p></item>
      <item><p>Untuk tak memaksimalkan suatu jendela memakai papan tik, tahan tombol <key href="help:gnome-help/keyboard-key-super">Super</key> dan tekan <key>↓</key>.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-tile-keyboard">
    <title>Jajarkan jendela memakai papan tik</title>
    
    <steps>
      <item><p>Untuk memaksimalkan suatu jendela sepanjang sisi kanan layar, tahan tombol <key href="help:gnome-help/keyboard-key-super">Super</key> dan tekan <key>→</key>.</p></item>
      <item><p>Untuk memaksimalkan suatu jendela sepanjang sisi kiri layar, tahan tombol <key href="help:gnome-help/keyboard-key-super">Super</key> dan tekan <key>←</key>.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-workspaces-keyboard">
    <title>Bertukar ruang kerja memakai papan tik</title>
    
    <steps>
    
    <item><p>Untuk memindah ke suatu ruang kerja di bawah yang saat ini, tekan <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Down</key></keyseq>.</p></item>
    <item><p>Untuk memindah ke suatu ruang kerja di atas yang saat ini, tekan <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Up</key></keyseq>.</p></item>

    </steps>
    
  </section>

</page>
