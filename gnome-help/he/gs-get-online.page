<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-get-online" xml:lang="he">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">גישה לאינטרנט</title>
    <link type="seealso" xref="net"/>
    <title role="seealso" type="link">הדרכה עבור גישה לאינטרנט</title>
    <link type="next" xref="gs-browse-web"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Niv Baehr</mal:name>
      <mal:email>bloop93@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>גישה לאינטרנט</title>
  
  <note style="important">
      <p>ניתן לראות את מצב החיבור לרשת בצדו השמאלי של הלוח העליון.</p>
  </note>  

  <section id="going-online-wired">

    <title>התחברות לרשת קווית.</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online1.svg" width="100%"/>

    <steps>
      <item><p>סמליל החיבור לרשת בצדו שמאל של הלוח העליון מראה מצב לא מקוון.</p>
      <p>המצב הלא מקוון עלול להיגרם ממספר סיבות: לדוגמה, כבל הרשת נותק, המחשב הוגדר לפעול ב<em>מצב טיסה</em>, או שאין רשתות אלחוטיות זמינות באזור.</p>
      <p>אם אתם משתמשים בחיבור קווי, עליכם רק לחבר את כבל הרשת על מנת להתחבר. המחשב ינסה להגדיר עבורכם את החיבור לרשת באופן אוטומטי.</p>
      <p>בעת שהמחשב מגדיר עבורכם את החיבור לרשת, סמליל החיבור לרשת יציג שלוש נקודות.</p></item>
      <item><p>כאשר החיבור לרשת הוגדר בהצלחה, סמליל החיבור לרשת יתחלף לסמל של מחשב המחובר לרשת.</p></item>
    </steps>
    
  </section>

  <section id="going-online-wifi">

    <title>התחברות לרשת אלחוטית</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online2.svg" width="100%"/>
    
    <steps>
      <title>להתחברות לרשת אלחוטית (Wi-Fi):</title>
      <item>
        <p>לחצו על <gui xref="shell-introduction#yourname">תפריט המערכת</gui> בצדו השמאלי של הלוח העליון.</p>
      </item>
      <item>
        <p>בחרו <gui>Wi-Fi לא מחובר</gui>. אזור הרשת אלחוטית בתפריט יתרחב.</p>
      </item>
      <item>
        <p>לחצו על <gui>בחירת רשת</gui>.</p>
      </item>
    </steps>

     <note style="important">
       <p>ניתן להתחבר לרשתות אלחוטיות רק אם חמרת המחשב תומכת באפשרות זו והמחשב נמצא באזור עם כיסוי של רשתות אלחוטיות.</p>
     </note>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online3.svg" width="100%"/>

    <steps style="continues">
    <item><p>מרשימת הרשתות האלחוטיות הזמינות, בחרו את הרשת אליה ברצונכם להתחבר, לאחר מכן לחצו <gui>התחברות</gui> לאישור.</p>
    <p>בהתאם להגדרות הרשת, יתכן ותתבקשו להזין את אישורי הרשת.</p></item>
    </steps>

  </section>

</page>
