<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="getting-started" version="1.0 if/1.0" xml:lang="he">

<info>
    <link type="guide" xref="index" group="gs"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <desc>חדשים ב־GNOME? למדו כיצד להסתדר.</desc>
    <title type="link">Getting started with GNOME</title>
    <title type="text">צעדים ראשונים</title>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Niv Baehr</mal:name>
      <mal:email>bloop93@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

<title>צעדים ראשונים</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg">
      <ui:caption its:translate="yes"><desc style="center">הפעלת יישומים</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>הפעלת יישומים</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>הזיזו את הסמן אל <gui>פעילויות</gui> בפינה הימנית העליונה של המסך.</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p>לחצו על הצלמית <gui>הצגת יישומים</gui></tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>לחצו על היישום שברצונכם להריץ, למשל, עזרה.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>לחלופין, פתחו את <gui>סקירת הפעילויות</gui> באמצעות המקלדת, על ידי לחיצה על מקש ה־<key href="help:gnome-help/keyboard-key-super">Super</key>.</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>התחילו להקליד את שם היישום אותו ברצונכם להפעיל.</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>הקישו <key>Enter</key> להפעלת היישום.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

</if:when>
</if:choose>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg">
        <ui:caption its:translate="yes"><desc style="center">מעבר בין משימות</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>מעבר בין משימות</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">הזיזו את הסמן אל <gui>פעילויות</gui> בפינה הימנית העליונה של המסך.</tt:p>
         </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>לחצו על חלון על מנת לעבור למשימה זו</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>כאשר מחצית המסך תודגש, שחררו את החלון.</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>To maximize a window along the right side, grab the window’s
            titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>כאשר מחצית המסך תודגש, שחררו את החלון.</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p>לחצו <keyseq> <key href="help:gnome-help/keyboard-key-super">Super</key><key> Tab</key></keyseq> להצגת <gui>מחליף החלונות</gui>.</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>שחררו את מקש ה־<key href="help:gnome-help/keyboard-key-super">Super </key> כדי לבחור את החלון המודגש הבא.</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>למעבר בין החלונות הפתוחים, אין לשחרר את <key href="help:gnome-help/keyboard-key-super">Super</key> אלא יש להחזיקו לחוץ, ולהקיש <key>Tab</key>.</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p>לחצו על מקש ה־<key href="help:gnome-help/keyboard-key-super">Super </key> להצגת <gui>סקירת הפעילויות</gui>.</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>התחילו להקליד את שם היישום אליו ברצונכם לעבור.</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>כאשר היישום מוצג כתוצאה הראשונה, לחצו <key> Enter</key> על מנת לעבור אליו.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg">
          <ui:caption its:translate="yes"><desc style="center">שימוש בחלונות ובשולחנות עבודה</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>חלונות ושולחנות עבודה</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>To maximize a window, grab the window’s titlebar and drag it to
            the top of the screen.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>כאשר המסך יודגש, שחררו את החלון.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>To unmaximize a window, grab the window’s titlebar and drag it
            away from the edges of the screen.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>ניתן לגרור את החלון ולשחזר את גודלו גם על ידי לחיצה על הלוח העליון.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>כאשר מחצית המסך תודגש, שחררו את החלון.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>To maximize a window along the right side of the screen, grab
            the window’s titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>כאשר מחצית המסך תודגש, שחררו את החלון.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>להגדלת חלון באמצעות המקלדת, החזיקו את מקש ה־<key href="help:gnome-help/keyboard-key-super">Super</key> והקישו <key>↑</key>.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>לשחזור גודל החלון, החזיקו את מקש ה־<key href="help:gnome-help/keyboard-key-super">Super</key> והקישו <key>↓</key>.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>להגדלת חלון לאורך צדו הימני של המסך, החזיקו את מקש ה־<key href="help:gnome-help/keyboard-key-super">Super</key> והקישו <key>→</key>.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>להגדלת חלון לאורך צדו השמאלי של המסך, החזיקו את מקש ה־<key href="help:gnome-help/keyboard-key-super">Super</key> והקישו <key>←</key>.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>למעבר לשולחן עבודה הנמצא מתחת לשולחן העבודה הנוכחי, לחצו <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Down</key></keyseq>.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>למעבר לשולחן עבודה הנמצא מעל לשולחן העבודה הנוכחי, לחצו <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Up</key></keyseq>.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

<links type="topic" style="grid" groups="tasks">
<title style="heading">Common tasks</title>
</links>

<links type="guide" style="heading nodesc">
<title/>
</links>

</page>
