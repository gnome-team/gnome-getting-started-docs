<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-switch-tasks" version="1.0 if/1.0" xml:lang="sv">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">Växla mellan uppgifter</title>
    <link type="seealso" xref="shell-windows-switching"/>
    <title role="seealso" type="link">En handledning i att växla mellan uppgifter</title>
    <link type="next" xref="gs-use-windows-workspaces"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Växla uppgifter</title>

  <ui:overlay width="812" height="452">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Växlar uppgifter</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">Flytta din musmarkör till <gui>Aktivitetshörnet</gui> längst upp till vänster på skärmen.</tt:p>
           </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>Klicka på ett fönster för att byta till den uppgiften.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>För att maximera ett fönster längs skärmens vänstra sida, fånga fönstrets namnlist och dra det åt vänster.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>När halva skärmen är markerad, släpp fönstret.</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>För att maximera ett fönster längs den högra sidan, fånga fönstrets namnlist och dra det åt höger.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>När halva skärmen är markerad, släpp fönstret.</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p>Tryck <keyseq> <key href="help:gnome-help/keyboard-key-super">Super</key><key> Tabb</key></keyseq> för att visa <gui>fönsterväxlaren</gui>.</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>Släpp <key href="help:gnome-help/keyboard-key-super">Super</key> för att välja nästa markerade fönster.</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>För att bläddra igenom listan av öppna fönster, släpp inte <key href="help:gnome-help/keyboard-key-super">Super</key> utan håll den nertryckt och tryck på <key>Tabb</key>.</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p>Tryck på <key href="help:gnome-help/keyboard-key-super">Super </key> för att visa <gui>Aktivitetsöversikt</gui>.</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>Börja skriv in namnet på det program du vill växla till.</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>När programmet visas som det första resultatet, tryck på <key>Retur</key> för att växla till det.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <section id="switch-tasks-overview">
    <title>Växla uppgifter</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <steps>
    <item><p>Flytta din musmarkör till <gui>Aktiviteter</gui>-hörnet längst upp till vänster på skärmen för att via <gui>Aktivitetsöversikt</gui> där du kan se alla de körande uppgifterna visas som små fönster.</p></item>
     <item><p>Klicka på ett fönster för att byta till den uppgiften.</p></item>
  </steps>

</if:when>
<if:when test="platform:gnome-classic">

  <steps>
    <item><p>Du kan växla mellan uppgifter genom att använda <gui>fönsterlistan</gui> i botten på skärmen. Öppna uppgifter visas som knappar i <gui>fönsterlistan</gui>.</p></item>
     <item><p>Klicka på en knapp i <gui>fönsterlistan</gui> för att växla till den uppgiften.</p></item>
  </steps>

</if:when>
</if:choose>

  </section>

  <section id="switching-tasks-tiling">
    <title>Lägg fönster sida-vid-sida</title>
    
    <steps>
      <item><p>För att maximera ett fönster längs en sida av skärmen, fånga fönstrets namnlist och dra det till vänster eller höger sida av skärmen.</p></item>
      <item><p>När hälften av skärmen är markerad, släpp fönstret för att maximera det längs den valda sidan av skärmen.</p></item>
      <item><p>För att maximera två fönster sida-vid-sida, fånga namnlisten på det andra fönstret och dra det till den andra sidan av skärmen.</p></item>
       <item><p>När hälften av skärmen är markerad, släpp fönstret för att maximera det längs den andra sidan av skärmen.</p></item>
    </steps>
    
  </section>
  
  <section id="switch-tasks-windows">
    <title>Växla mellan fönster</title>
    
  <steps>
   <item><p>Tryck på <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Tabb</key></keyseq> för att visa <gui>fönsterväxlaren</gui>, som listar alla öppna fönster.</p></item>
   <item><p>Släpp <key href="help:gnome-help/keyboard-key-super">Super</key> för att välja nästa markerade fönster i <gui>fönsterväxlaren</gui>.</p>
   </item>
   <item><p>För att bläddra igenom listan av öppna fönster, släpp inte <key href="help:gnome-help/keyboard-key-super">Super</key> utan håll den nertryckt och tryck på <key>Tabb</key>.</p></item>
  </steps>

  </section>

  <section id="switch-tasks-search">
    <title>Använd sökning för att växla mellan program</title>
    
    <steps>
      <item><p>Tryck på <key href="help:gnome-help/keyboard-key-super">Super</key> för att visa <gui>Aktivitetsöversikt</gui>.</p></item>
      <item><p>Bara börja skriv namnet på det program du vill växla till. Program som matchar det du har skrivit kommer att visas medan du skriver.</p></item>
      <item><p>När programmet som du vill växla till visas som det första resultatet, tryck på <key>Retur</key> för att växla till det.</p></item>
      
    </steps>
    
  </section>

</page>
