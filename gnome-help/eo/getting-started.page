<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="getting-started" version="1.0 if/1.0" xml:lang="eo">

<info>
    <link type="guide" xref="index" group="gs"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <desc>Ĉu nova al GNOME? Lernu kiel uzi.</desc>
    <title type="link">Komenci kun GNOME</title>
    <title type="text">Komenci</title>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Carmen Bianca BAKKER</mal:name>
      <mal:email>carmen@carmenbianca.eu</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

<title>Komenci</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg">
      <ui:caption its:translate="yes"><desc style="center">Lanĉi aplikaĵojn</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Lanĉi aplikaĵojn</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>Movu vian musmontrilon al la <gui>Aktivecoj</gui>-angulo ĉe la supra maldekstro de la ekrano.</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p>Alklaku la <gui>Montri aplikaĵojn</gui>-bildsimbolon.</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>Alklaku la aplikaĵon, kiun vi volas lanĉi, ekzemple, Helpo.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>Alternative, uzu la klavaron por malfermi la <gui>Aktivecoj-Superrigardo</gui> per premi la <key href="help:gnome-help/keyboard-key-super">Super</key>-klavon.</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>Komencu tajpi la nomon de la aplikaĵo, kiun vi volas lanĉi.</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>Premu <key>Enter</key> por lanĉi la aplikaĵon.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

</if:when>
</if:choose>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg">
        <ui:caption its:translate="yes"><desc style="center">Ŝanĝi taskojn</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Ŝanĝi taskojn</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">Movu vian musmontrilon al la <gui>Aktivecoj</gui>-angulo ĉe la supra maldekstro de la ekrano.</tt:p>
         </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>Alklaku fenestron por ŝanĝi al tiu tasko.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>Por maksimumigi fenestron ĉe la maldekstra flanko de la ekrano, kaptu la titolbreton de la fenestro kaj ŝovu ĝin maldekstren.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>Kiam duono de la ekrano estas emfazita, malkaptu la fenestron.</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>Por maksimumigi fenestron ĉe la dekstra flanko, kaptu la titolbreton de la fenestro kaj ŝovu ĝin dekstren.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>Kiam duono de la ekrano estas emfazita, malkaptu la fenestron.</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p>Premu <keyseq> <key href="help:gnome-help/keyboard-key-super">Super</key><key> Tab</key></keyseq> por montri la <gui>fenestra ŝanĝilo</gui>.</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>Malkaptu <key href="help:gnome-help/keyboard-key-super">Super </key> por elekti la sekvan emfazitan fenestron.</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>Por trairi tra la listo de malfermaj fenestroj, ne malkaptu <key href="help:gnome-help/keyboard-key-super">Super</key> sed tenu ĝin, kaj premu <key>Tab</key>.</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p>Premu la <key href="help:gnome-help/keyboard-key-super">Super </key>-klavon por montri la <gui>Aktivecoj-Superrigardon</gui>.</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>Komencu tajpi la nomon de la aplikaĵo al kiu vi volas ŝanĝi.</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>Kiam aperas la aplikaĵo kiel la unua rezulto, premu <key>Enter</key> por ŝanĝi al ĝi.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg">
          <ui:caption its:translate="yes"><desc style="center">Uzi fenestrojn kaj laborspacojn</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Fenestroj kaj laborspacoj</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>Por maksimumigi fenestron, kaptu la titolbreton de la fenestro kaj ŝovu ĝin al la supro de la ekrano.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>Kiam la ekrano estas emfazita, malkaptu la fenestron.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>Por malmaksimumigi fenestron, kaptu la titolbreton de la fenestro kaj ŝovu ĝin for de la rando de la ekrano.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>Vi povas ankaŭ alklaki la supran breton por ŝovi la fenestron for kaj malmaksimumigi ĝin.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>Por maksimumigi fenestron ĉe la maldekstra flanko de la ekrano, kaptu la titolbreton de la fenestro kaj ŝovu ĝin maldekstren.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>Kiam duono de la ekrano estas emfazita, malkaptu la fenestron.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>Por maksimumigi fenestron ĉe la dekstra flanko de la ekrano, kaptu la titolbreton de la fenestro kaj ŝovu ĝin dekstren.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>Kiam duono de la ekrano estas emfazita, malkaptu la fenestron.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>Por maksimumigi fenestron uzante la klavaron, premu la <key href="help:gnome-help/keyboard-key-super">Super</key>-klavon kaj premu <key>↑</key>.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>Por restaŭri la fenestron al ĝia nemaksimumigita grandeco, premu la <key href="help:gnome-help/keyboard-key-super">Super</key>-klavon kaj premu <key>↓</key>.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>Por maksimumigi fenestron ĉe la dekstra flanko de la ekrano, premu la <key href="help:gnome-help/keyboard-key-super">Super</key>-klavon kaj premu <key>→</key>.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>Por maksimumigi fenestron ĉe la maldekstra flanko de la ekrano, premu la <key href="help:gnome-help/keyboard-key-super">Super</key>-klavon kaj premu <key>←</key>.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>Por movi al laborspaco kiu estas sub la aktuala laborspaco, premu <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Suba Paĝo</key></keyseq>.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>Por movi al laborspaco kiu estas super la aktuala laborspaco, premu <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Supra Paĝo</key></keyseq>.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

<links type="topic" style="grid" groups="tasks">
<title style="heading">Oftaj taskoj</title>
</links>

<links type="guide" style="heading nodesc">
<title/>
</links>

</page>
