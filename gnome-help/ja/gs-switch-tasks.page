<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-switch-tasks" version="1.0 if/1.0" xml:lang="ja">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">タスクを切り替える</title>
    <link type="seealso" xref="shell-windows-switching"/>
    <title role="seealso" type="link">タスクの切り替えに関するチュートリアル</title>
    <link type="next" xref="gs-use-windows-workspaces"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2013, 2015, 2016</mal:years>
    </mal:credit>
  </info>

  <title>タスクを切り替える</title>

  <ui:overlay width="812" height="452">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>タスクの切り替え</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">マウスポインターを画面左上隅の<gui>アクティビティ</gui>まで移動させます。</tt:p>
           </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>ウィンドウをクリックすると、そのタスクに切り替わります。</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>画面半分がハイライト表示されたら、ウィンドウを離します。</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>To maximize a window along the right side, grab the window’s
            titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>画面半分がハイライト表示されたら、ウィンドウを離します。</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p><keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Tab</key></keyseq> を押して、ウィンドウスイッチャーを表示します。</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>次のウィンドウがハイライト表示され、<key href="help:gnome-help/keyboard-key-super">Super</key> キーを離すと、そのウィンドウが選択されます。</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>開いているウィンドウを順に切り替えるには、<key href="help:gnome-help/keyboard-key-super">Super</key> を離さずに押したままで <key>Tab</key> を押します。</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p><key href="help:gnome-help/keyboard-key-super">Super</key> キーを押して<gui>アクティビティ画面</gui>を開きます。</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>切り替え先のアプリケーション名をキーボードでタイプします。</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>アプリケーションが検索結果の先頭に表示されたら、<key>Enter</key> を押すと、そのアプリケーションに切り替わります。</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <section id="switch-tasks-overview">
    <title>タスクを切り替える</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <steps>
    <item><p>マウスポインターを画面左上隅の<gui>アクティビティ</gui>まで移動させ、<gui>アクティビティ画面</gui>を開きます。アクティビティ画面には、現在実行中のタスクが小さなウィンドウで表示されます。</p></item>
     <item><p>ウィンドウをクリックすると、そのタスクに切り替わります。</p></item>
  </steps>

</if:when>
<if:when test="platform:gnome-classic">

  <steps>
    <item><p>画面下部の<gui>ウィンドウ一覧</gui>を使って、タスクを切り替えることができます。</p></item>
     <item><p><gui>ウィンドウ一覧</gui>の中から切り替え先のタスクを選択します。</p></item>
  </steps>

</if:when>
</if:choose>

  </section>

  <section id="switching-tasks-tiling">
    <title>ウィンドウを左右に並べる</title>
    
    <steps>
      <item><p>To maximize a window along a side of the screen, grab the window’s
       titlebar and drag it to the left or right side of the screen.</p></item>
      <item><p>画面半分がハイライト表示されたら、ウィンドウを離すと、ドラッグした側半分に最大化します。</p></item>
      <item><p>ふたつのウィンドウを左右片側ずつに最大化するには、ふたつ目のウィンドウのタイトルバーをつかんで反対側の端までドラッグします。</p></item>
       <item><p>画面半分がハイライト表示されたら、ウィンドウを離すと、ドラッグした側半分に最大化します。</p></item>
    </steps>
    
  </section>
  
  <section id="switch-tasks-windows">
    <title>ウィンドウを切り替える</title>
    
  <steps>
   <item><p><keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Tab</key></keyseq> を押すと、<gui>ウィンドウスイッチャー</gui>が表示されます。ウィンドウスイッチャーには、現在開いているウィンドウの一覧が表示されます。</p></item>
   <item><p><gui>ウィンドウスイッチャー</gui>上の次のウィンドウがハイライト表示され、<key href="help:gnome-help/keyboard-key-super">Super</key> キーを離すと、そのウィンドウが選択されます。</p>
   </item>
   <item><p>開いているウィンドウを順に切り替えるには、<key href="help:gnome-help/keyboard-key-super">Super</key> を離さずに押したままで <key>Tab</key> を押します。</p></item>
  </steps>

  </section>

  <section id="switch-tasks-search">
    <title>検索機能を使ってアプリケーションを切り替える</title>
    
    <steps>
      <item><p><key href="help:gnome-help/keyboard-key-super">Super</key> キーを押して<gui>アクティビティ画面</gui>を開きます。</p></item>
      <item><p>切り替え先のアプリケーション名をキーボードでタイプします。マッチするアプリケーションが表示されます。</p></item>
      <item><p>切り替え先のアプリケーションが検索結果の先頭に表示されたら、<key>Enter</key> を押すと、そのアプリケーションに切り替わります。</p></item>
      
    </steps>
    
  </section>

</page>
