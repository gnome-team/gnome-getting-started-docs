<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-get-online" xml:lang="as">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>ইয়াকুব স্টাইনাৰ</name>
    </credit>
    <credit type="author">
      <name>পিটাৰ ক'ভাৰ</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">অনলাইন হওক</title>
    <link type="seealso" xref="net"/>
    <title role="seealso" type="link">অনলাইন হবলৈ এটা শিক্ষা</title>
    <link type="next" xref="gs-browse-web"/>
  </info>

  <title>অনলাইন হওক</title>
  
  <note style="important">
      <p>আপুনি আপোনাৰ নেটৱাৰ্ক সংযোগৰ অৱস্থা ওপৰ বাৰৰ সোঁ-ফালে দেখা পাব।</p>
  </note>  

  <section id="going-online-wired">

    <title>এটা তাঁৰযুক্ত নেটৱাৰ্কৰ সৈতে সংযোগ কৰক</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online1.svg" width="100%"/>

    <steps>
      <item><p>The network connection icon on the right-hand side of the top
       bar shows that you are offline.</p>
      <p>The offline status can be caused by a number of reasons: for
       example, a network cable has been unplugged, the computer has been set
       to run in <em>airplane mode</em>, or there are no available Wi-Fi
       networks in your area.</p>
      <p>যদি আপুনি এটা তাঁৰযুক্ত নেটৱাৰ্ক ব্যৱহাৰ কৰিব বিচাৰে, অনলাইন হ'বলৈ এটা নেটৱাৰ্ক কেবুল প্লাগ কৰক। কমপিউটাৰে আপোনাৰ বাবে স্বচালিতভাৱে নেটৱাৰ্ক সংযোগ সংস্থাপন কৰাৰ চেষ্টা কৰিব।</p>
      <p>যেতিয়া কমপিউটাৰে আপোনাৰ বাবে এটা নেটৱাৰ্ক সংযোগ সংস্থাপন কৰি থাকে, নেটৱাৰ্ক সংযোগ আইকনে তিনিটা বিন্দু দেখুৱাব।</p></item>
      <item><p>যেতিয়া নেটৱাৰ্ক সংযোগ সফলভাৱে সংস্থাপন হৈ যায়, নেটৱাৰ্ক সংযোগ আইকন নেটৱাৰ্ক কমপিউটাৰ চিহ্নলৈ পৰিবৰ্তন হৈ যায়।</p></item>
    </steps>
    
  </section>

  <section id="going-online-wifi">

    <title>এটা Wi-Fi নেটৱাৰ্কৰ সৈতে সংযোগ কৰক</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online2.svg" width="100%"/>
    
    <steps>
      <title>To connect to a Wi-Fi (wireless) network:</title>
      <item>
        <p>Click the <gui xref="shell-introduction#yourname">system menu</gui>
        on the right side of the top bar.</p>
      </item>
      <item>
        <p>Select <gui>Wi-Fi Not Connected</gui>. The Wi-Fi section of the menu
        will expand.</p>
      </item>
      <item>
        <p>Click <gui>Select Network</gui>.</p>
      </item>
    </steps>

     <note style="important">
       <p>আপুনি এটা Wi-Fi নেটৱাৰ্কৰ সৈতে কেৱল তেতিয়াহে সংযোগ কৰিব পাৰিব যদি আপোনাৰ কমপিউটাৰে ইয়াক সমৰ্থন কৰে আৰু আপুনি Wi-Fi পৰিধিৰ স্থানত থাকে।</p>
     </note>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online3.svg" width="100%"/>

    <steps style="continues">
    <item><p>উপলব্ধ Wi-Fi নেটৱাৰ্কসমূহৰ তালিকাৰ পৰা, আপুনি সংযোগ কৰিব বিচৰা নেটৱাৰ্ক বাছক, আৰু সুনিশ্চিত কৰিবলৈ <gui>সংযোগ কৰক</gui> ক্লিক কৰক।</p>
    <p>নেটৱাৰ্ক সংযোগৰ ওপৰত নিৰ্ভৰ কৰি, আপোনাক নেটৱাৰ্ক তথ্যসমূহৰ বাবে প্ৰমপ্ট কৰা হব পাৰে।</p></item>
    </steps>

  </section>

</page>
