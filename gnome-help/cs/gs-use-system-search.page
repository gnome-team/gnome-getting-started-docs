<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-system-search" xml:lang="cs">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovář</name>
    </credit>
    <credit type="author">
      <name>Hannie Dumoleyn</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">Vyhledávání v systému</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">Vysvětlení, jak používat vyhledávání v systému</title>
    <link type="next" xref="gs-get-online"/>
  </info>

  <title>Vyhledávání v systému</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-search1.svg" width="100%"/>
    
    <steps>
    <item><p>Otevřete přehled <gui>Činností</gui> kliknutím na <gui>Činnosti</gui> v levém horním rohu obrazovky, nebo zmáčknutím klávesy <key href="help:gnome-help/keyboard-key-super">Super</key>, a začněte hledat tím, že začnete psát.</p>
    <p>Výsledky vyhovující tomu, co jste napsali, se budou objevovat průběžně. První výsledek je vždy zvýrazněný a zobrazený nahoře.</p>
    <p>Zmáčknutím <key>Enter</key> se na tento první zvýrazněný výsledek přepnete.</p></item>
    </steps>
    
    <media its:translate="no" type="image" mime="image/svg" src="gs-search2.svg" width="100%"/>
    <steps style="continues">
      <item><p>Mezi položky, které se mohou objevit ve výsledcích hledání patří:</p>
      <list>
        <item><p>vyhovující aplikace (zobrazí se ve výsledcích jako první)</p></item>
        <item><p>vyhovující nastavení,</p></item>
        <item><p>vyhovující kontakty,</p></item>
        <item><p>vyhovující dokumenty,</p></item>
        <item><p>vyhovující kalendáře,</p></item>
        <item><p>vyhovující výpočty na kalkulačce,</p></item>
        <item><p>vyhovující software,</p></item>
        <item><p>vyhovující soubory,</p></item>
        <item><p>vyhovující terminály,</p></item>
        <item><p>vyhovující hesla a klíče.</p></item>
      </list>
      </item>
      <item><p>Kliknutím na položku ve výsledcích hledání se na ni přepnete.</p>
      <p>Případně položku vyberte pomocí klávesových šipek a zmáčkněte <key>Enter</key>.</p></item>
    </steps>

    <section id="use-search-inside-applications">
    
      <title>Vyhledávání v aplikacích</title>
      
      <p>Vyhledávání v systému sdružuje výsledky z různorodých aplikací. Po levé straně výsledků hledání můžete vidět ikony aplikací, které výsledky poskytly. Kliknutím na některou z těchto ikon spustíte opětovné hledání přímo v aplikaci patřící k dané ikoně. Protože v <gui>Přehledu činností</gui> se zobrazují jen nepřesnější shody, může vám hledání přímo v aplikaci přinést lepší výsledky.</p>

    </section>

    <section id="use-search-customize">

      <title>Přizpůsobení výsledků hledání</title>

      <media its:translate="no" type="image" mime="image/svg" src="gs-search-settings.svg" width="100%"/>

      <note style="important">
      <p>Počítač vám umožňuje si přizpůsobit, co chcete zobrazit ve výsledcích hledání v přehledu <gui>Činností</gui>. Například si můžete zvolit, jestli chcete zobrazovat výsledky pro webové stránky, fotografie nebo hudbu.</p>
      </note>


      <steps>
        <title>Když si chcete přizpůsobit, co se má zobrazovat ve výsledcích hledání:</title>
        <item><p>Klikněte na <gui xref="shell-introduction#yourname">systémovou nabídku</gui> v pravé části horní lišty.</p></item>
        <item><p>Klikněte na <gui>Nastavení</gui>.</p></item>
        <item><p>Klikněte na <gui>Hledání</gui> v panelu vlevo.</p></item>
        <item><p>V seznamu prohledávaných míst klikněte na přepínač vedle zdroje hledání, který chcete povolit či zakázat.</p></item>
      </steps>

    </section>

</page>
