<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="getting-started" version="1.0 if/1.0" xml:lang="kn">

<info>
    <link type="guide" xref="index" group="gs"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <desc>GNOME ಗೆ ಹೊಸಬರೆ? ಬಳಸುವುದನ್ನು ಸ್ಥೂಲವಾಗಿ ಅರಿತುಕೊಳ್ಳಿ.</desc>
    <title type="link">Getting started with GNOME</title>
    <title type="text">ಆರಂಭಿಸುವಿಕೆ</title>
</info>

<title>ಆರಂಭಿಸುವಿಕೆ</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg">
      <ui:caption its:translate="yes"><desc style="center">ಅನ್ವಯಗಳನ್ನು ಆರಂಭಿಸಿ</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>ಅನ್ವಯಗಳನ್ನು ಆರಂಭಿಸುವಿಕೆ</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>ನಿಮ್ಮ ಮೌಸ್‌ನ ಸೂಚಕವನ್ನು ತೆರೆಯ ಮೇಲ್ಭಾಗದ ಎಡಗಡೆ ಇರುವ <gui>ಚಟುವಟಿಕೆಗಳು</gui> ಮೂಲೆಗೆ ತೆಗೆದುಕೊಂಡುಹೋಗಿ.</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p><gui>ಅನ್ವಯಗಳನ್ನು ತೋರಿಸು</gui> ಚಿಹ್ನೆಯ ಮೇಲೆ ಕ್ಲಿಕ್ ಮಾಡಿ.</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>ನೀವು ಚಲಾಯಿಸಲು ಬಯಸುವ ಅನ್ವಯದ ಮೇಲೆ ಕ್ಲಿಕ್ ಮಾಡಿ, ಉದಾಹರಣೆಗೆ, ಸಹಾಯ.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>ಪರ್ಯಾಯವಾಗಿ, <gui>ಚಟುವಟಿಕೆಗಳ ಅವಲೋಕನ</gui>ವನ್ನು ತೆರೆಯಲು ಕೀಲಿಮಣೆಯಲ್ಲಿನ <key href="help:gnome-help/keyboard-key-super">Super</key> ಕೀಲಿಯನ್ನು ಒತ್ತಿ.</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>ನೀವು ಚಲಾಯಿಸಲು ಬಯಸುವ ಅನ್ವಯದ ಹೆಸರನ್ನು ನಮೂದಿಸಲು ಆರಂಭಿಸಿ.</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>ಅನ್ವಯವನ್ನು ಚಲಾಯಿಸಲು <key>Enter</key> ಅನ್ನು ಒತ್ತಿ.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

</if:when>
</if:choose>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg">
        <ui:caption its:translate="yes"><desc style="center">ಕಾರ್ಯಗಳನ್ನು ಬದಲಾಯಿಸಿ</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>ಕಾರ್ಯಗಳನ್ನು ಬದಲಾಯಿಸುವಿಕೆ</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">ನಿಮ್ಮ ಮೌಸ್‌ನ ಸೂಚಕವನ್ನು ತೆರೆಯ ಮೇಲ್ಭಾಗದ ಎಡಗಡೆ ಇರುವ <gui>ಚಟುವಟಿಕೆಗಳು</gui> ಮೂಲೆಗೆ ತೆಗೆದುಕೊಂಡುಹೋಗಿ.</tt:p>
         </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>ಆ ಕಾರ್ಯಕ್ಕೆ ಬದಲಾಯಿಸಲು ಒಂದು ಕಿಟಕಿ ಮೇಲೆ ಕ್ಲಿಕ್ ಮಾಡಿ.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>ತೆರೆಯನ್ನು ಅರ್ಧ ಹೈಲೈಟ್ ಮಾಡಿದಾಗ, ಕಿಟಕಿಯನ್ನು ಮುಕ್ತಗೊಳಿಸಿ.</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>To maximize a window along the right side, grab the window’s
            titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>ತೆರೆಯನ್ನು ಅರ್ಧ ಹೈಲೈಟ್ ಮಾಡಿದಾಗ, ಕಿಟಕಿಯನ್ನು ಮುಕ್ತಗೊಳಿಸಿ.</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p><gui>ಕಿಟಕಿ ಬದಲಾವಣೆಗಾರ</gui>ನನ್ನು ತೋರಿಸಲು <keyseq> <key href="help:gnome-help/keyboard-key-super">Super</key><key> Tab</key></keyseq> ಅನ್ನು ಒತ್ತಿ.</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>ಮುಂದಿನ ಹೈಲೈಟ್ ಮಾಡಿದ ಕಿಟಕಿಯನ್ನು ಆರಿಸಲು <key href="help:gnome-help/keyboard-key-super">Super </key> ಅನ್ನು ಮುಕ್ತಗೊಳಿಸಿ.</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>ತೆರೆಯಲಾದ ಕಿಟಕಿಗಳ ಪಟ್ಟಿಯ ಮುಖಾಂತರ ಹಾದುಹೋಗಲು, <key href="help:gnome-help/keyboard-key-super">Super</key> ಅನ್ನು ಬಿಡದೆ ಹಾಗೆಯೆ ಹಿಡಿದಿಟ್ಟುಕೊಳ್ಳಿ, ನಂತರ <key>Tab</key> ಅನ್ನು ಒತ್ತಿ.</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p><gui>ಚಟುವಟಿಕೆಗಳ ಅವಲೋಕನ</gui>ವನ್ನು ತೋರಿಸಲು <key href="help:gnome-help/keyboard-key-super">Super </key> ಕೀಲಿಯನ್ನು ಒತ್ತಿ.</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>ನೀವು ಬದಲಾಯಿಸಲು ಬಯಸುವ ಅನ್ವಯದ ಹೆಸರನ್ನು ನಮೂದಿಸಲು ಆರಂಭಿಸಿ.</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>ಅನ್ವಯವು ಮೊದಲನೆಯ ಫಲಿತಾಂಶವಾಗಿ ಕಾಣಿಸಿಕೊಂಡಾಗ, ಅದಕ್ಕೆ ಬದಲಾಯಿಸಲು <key> Enter</key> ಅನ್ನು ಒತ್ತಿ.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg">
          <ui:caption its:translate="yes"><desc style="center">ಕಿಟಕಿಗಳು ಮತ್ತು ಕಾರ್ಯಸ್ಥಳಗಳನ್ನು ಬಳಸಿ</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>ಕಿಟಕಿಗಳು ಮತ್ತು ಕಾರ್ಯಸ್ಥಳಗಳು</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>To maximize a window, grab the window’s titlebar and drag it to
            the top of the screen.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>ತೆರೆಯನ್ನು ಹೈಲೈಟ್ ಮಾಡಿದಾಗ, ಕಿಟಕಿಯನ್ನು ಮುಕ್ತಗೊಳಿಸಿ.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>To unmaximize a window, grab the window’s titlebar and drag it
            away from the edges of the screen.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>ಕಿಟಕಿಯನ್ನು ಎಳೆಯಲು ಮತ್ತು ಅದನ್ನು ಹಿಗ್ಗಿಸದಿರಲು ಅದರ ಮೇಲಿನ ಪಟ್ಟಿಯ ಮೇಲೂ ಸಹ ಕ್ಲಿಕ್ ಮಾಡಿ.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>ತೆರೆಯನ್ನು ಅರ್ಧ ಹೈಲೈಟ್ ಮಾಡಿದಾಗ, ಕಿಟಕಿಯನ್ನು ಮುಕ್ತಗೊಳಿಸಿ.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>To maximize a window along the right side of the screen, grab
            the window’s titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>ತೆರೆಯನ್ನು ಅರ್ಧ ಹೈಲೈಟ್ ಮಾಡಿದಾಗ, ಕಿಟಕಿಯನ್ನು ಮುಕ್ತಗೊಳಿಸಿ.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>ಕೀಲಿಮಣೆಯನ್ನು ಬಳಸಿಕೊಂಡು ಕಿಟಕಿಯನ್ನು ಹಿಗ್ಗಿಸಲು, <key href="help:gnome-help/keyboard-key-super">Super</key>ಕೀಲಿಯನ್ನು ಒತ್ತಿಹಿಡಿದು ನಂತರ <key>↑</key> ಅನ್ನು ಒತ್ತಿ.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>ಕಿಟಕಿಯನ್ನು ಅದರ ಹಿಗ್ಗಿಸದಿರುವ ಸ್ಥಿತಿಗೆ ಮರಳಿಸ್ಥಾಪಿಸಲು, <key href="help:gnome-help/keyboard-key-super">Super</key> ಕೀಲಿಯನ್ನು ಒತ್ತಿಹಿಡಿದು ನಂತರ <key>↓</key> ಅನ್ನು ಒತ್ತಿ.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>ತೆರೆಯ ಬಲಗಡೆಯಿಂದ ಕಿಟಕಿಯನ್ನು ಹಿಗ್ಗಿಸಲು, <key href="help:gnome-help/keyboard-key-super">Super</key> ಕೀಲಿಯನ್ನು ಒತ್ತಿಹಿಡಿದು ನಂತರ <key>→</key> ಅನ್ನು ಒತ್ತಿ.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>ತೆರೆಯ ಎಡಗಡೆಯಿಂದ ಕಿಟಕಿಯನ್ನು ಹಿಗ್ಗಿಸಲು, <key href="help:gnome-help/keyboard-key-super">Super</key> ಕೀಲಿಯನ್ನು ಒತ್ತಿಹಿಡಿದು ನಂತರ <key>←</key> ಅನ್ನು ಒತ್ತಿ.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>ಪ್ರಸಕ್ತ ಕಾರ್ಯಸ್ಥಳದ ಕೆಳಗಿರುವ ಒಂದು ಕಾರ್ಯಸ್ಥಳಕ್ಕೆ ಸ್ಥಳಾಂತರಿಸಲು, <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Down</key></keyseq> ಅನ್ನು ಒತ್ತಿ.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>ಪ್ರಸಕ್ತ ಕಾರ್ಯಸ್ಥಳದ ಮೇಲಿರುವ ಒಂದು ಕಾರ್ಯಸ್ಥಳಕ್ಕೆ ಸ್ಥಳಾಂತರಿಸಲು, <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Down</key></keyseq> ಅನ್ನು ಒತ್ತಿ</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

<links type="topic" style="grid" groups="tasks">
<title style="heading">Common tasks</title>
</links>

<links type="guide" style="heading nodesc">
<title/>
</links>

</page>
