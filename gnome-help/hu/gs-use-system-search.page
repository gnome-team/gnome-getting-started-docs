<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-system-search" xml:lang="hu">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <credit type="author">
      <name>Hannie Dumoleyn</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">Rendszerkeresés használata</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">Ismertető a rendszerkeresés használatához</title>
    <link type="next" xref="gs-get-online"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2013, 2015, 2018.</mal:years>
    </mal:credit>
  </info>

  <title>Rendszerkeresés használata</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-search1.svg" width="100%"/>
    
    <steps>
    <item><p>Nyissa meg a <gui>Tevékenységek</gui> áttekintést képernyő bal felső sarkában lévő <gui>Tevékenységek</gui> menüre kattintva, vagy a <key href="help:gnome-help/keyboard-key-super">Super</key> billentyű megnyomásával. Kezdjen el gépelni a kereséshez.</p>
    <p>A begépelt szövegnek megfelelő találatok megjelennek a gépelés közben. Az első találat mindig kiemelve, felül jelenik meg.</p>
    <p>Az első kiemelt találatra váltáshoz nyomja meg az <key>Enter</key> billentyűt.</p></item>
    </steps>
    
    <media its:translate="no" type="image" mime="image/svg" src="gs-search2.svg" width="100%"/>
    <steps style="continues">
      <item><p>A találatok közt megjelenhetnek:</p>
      <list>
        <item><p>a megfelelő alkalmazások, a találatok listájának elején,</p></item>
        <item><p>a megfelelő beállítások,</p></item>
        <item><p>a megfelelő névjegyek, és</p></item>
        <item><p>a megfelelő dokumentumok,</p></item>
        <item><p>a megfelelő naptár,</p></item>
        <item><p>a megfelelő számológép,</p></item>
        <item><p>a megfelelő szoftverek,</p></item>
        <item><p>a megfelelő fájlok,</p></item>
        <item><p>a megfelelő terminál,</p></item>
        <item><p>a megfelelő jelszavak és kulcsok.</p></item>
      </list>
      </item>
      <item><p>A találatok közt kattintson egy elemre az arra való átváltáshoz.</p>
      <p>Egy elemet a nyílbillentyűkkel is kiemelhet, és megnyomhatja az <key>Enter</key> billentyűt az átváltáshoz.</p></item>
    </steps>

    <section id="use-search-inside-applications">
    
      <title>Keresés az alkalmazásokon belül</title>
      
      <p>A rendszer keresője a különböző alkalmazásokból származó találatokat gyűjti. A találatok listájának bal oldalán láthatja azon alkalmazások ikonjait, amelyekből a találatok származnak. Kattintson az egyik ikonra a keresés újraindításához az adott ikonhoz tartozó alkalmazáson belülről. Mivel a <gui>Tevékenységek áttekintés</gui> csak a legpontosabb találatokat mutatja, az alkalmazáson belüli keresés bővebb találati listát adhat.</p>

    </section>

    <section id="use-search-customize">

      <title>Találatok személyre szabása</title>

      <media its:translate="no" type="image" mime="image/svg" src="gs-search-settings.svg" width="100%"/>

      <note style="important">
      <p>A számítógépe lehetővé teszi a <gui>Tevékenységek áttekintésben</gui> megjelenő találatok személyre szabását. Eldöntheti például, hogy szeretné-e megjeleníteni a weboldalak, fényképek vagy zenék közti találatokat.</p>
      </note>


      <steps>
        <title>A találatok személyre szabásához:</title>
        <item><p>Kattintson a <gui xref="shell-introduction#yourname">rendszer menüre</gui> a felső sáv jobb oldalán.</p></item>
        <item><p>Click <gui>Settings</gui>.</p></item>
        <item><p>Kattintson a <gui>Keresés</gui> elemre a bal oldali panelen.</p></item>
        <item><p>A keresési helyek listájában kattintson a kapcsolóra az engedélyezni vagy letiltani kívánt keresési hely mellett.</p></item>
      </steps>

    </section>

</page>
