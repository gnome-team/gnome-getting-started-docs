<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-windows-workspaces" xml:lang="el">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">Χρήση παραθύρων και χώρων εργασίας</title>
    <link type="seealso" xref="shell-windows-switching"/>
    <title role="seealso" type="link">Ένα μάθημα στη χρήση παραθύρων και χώρων εργασίας</title>
    <link type="next" xref="gs-use-system-search"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2013, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gmail.com</mal:email>
      <mal:years>2012, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Χρήση παραθύρων και χώρων εργασίας</title>

  <ui:overlay width="812" height="452">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Παράθυρα και χώροι εργασίας</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>To maximize a window, grab the window’s titlebar and drag it to
            the top of the screen.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>Όταν η οθόνη επισημανθεί, ελευθερώστε το παράθυρο.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>To unmaximize a window, grab the window’s titlebar and drag it
            away from the edges of the screen.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>Μπορείτε επίσης να πατήσετε την πάνω γραμμή για να μεταφέρετε το παράθυρο μακριά και να το απομεγιστοποιήσετε.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>Όταν η μισή οθόνη επισημανθεί, ελευθερώστε το παράθυρο.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>To maximize a window along the right side of the screen, grab
            the window’s titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>Όταν η μισή οθόνη επισημανθεί, ελευθερώστε το παράθυρο.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>Για μεγιστοποίηση ενός παραθύρου χρησιμοποιώντας το πληκτρολόγιο, κρατήστε πατημένο το πλήκτρο <key href="help:gnome-help/keyboard-key-super">Λογότυπο</key> και πατήστε <key>↑</key>.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>Για επαναφορά του παραθύρου στο αμεγιστοποίητο μέγεθος του, κρατήστε πατημένο το πλήκτρο <key href="help:gnome-help/keyboard-key-super">Λογότυπο</key> και πατήστε <key>↓</key>.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>Για μεγιστοποίηση ενός παραθύρου κατά μήκος της δεξιάς πλευράς της οθόνης, κρατήστε πατημένο το πλήκτρο <key href="help:gnome-help/keyboard-key-super">Λογότυπο</key> και πατήστε <key>→</key>.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>Για μεγιστοποίηση ενός παραθύρου κατά μήκος της αριστερής πλευράς της οθόνης, κρατήστε πατημένο το πλήκτρο <key href="help:gnome-help/keyboard-key-super">Λογότυπο</key> και πατήστε <key>←</key>.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>Για μετακίνηση σε έναν χώρο εργασίας που είναι κάτω από τον τρέχοντα χώρο εργασίας, πατήστε το πλήκτρο <keyseq><key href="help:gnome-help/keyboard-key-super">Λογότυπο </key><key>Page Down</key></keyseq>.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>Για μετακίνηση σε έναν χώρο εργασίας που είναι πάνω από τον τρέχοντα χώρο εργασίας, πατήστε το πλήκτρο <keyseq><key href="help:gnome-help/keyboard-key-super">Λογότυπο </key><key>Page Up</key></keyseq>.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>
  
  <section id="use-workspaces-and-windows-maximize">
    <title>Μεγιστοποίηση και απομεγιστοποίηση παραθύρων</title>
    <p/>
    
    <steps>
      <item><p>To maximize a window so that it takes up all of the space on
       your desktop, grab the window’s titlebar and drag it to the top of the
       screen.</p></item>
      <item><p>Όταν η οθόνη επισημαίνεται, ελευθερώστε το παράθυρο για να το μεγιστοποιήσετε.</p></item>
      <item><p>To restore a window to its unmaximized size, grab the window’s
       titlebar and drag it away from the edges of the screen.</p></item>
    </steps>
    
  </section>

  <section id="use-workspaces-and-windows-tile">
    <title>Παράθεση παραθύρων</title>
    <p/>
    
    <steps>
      <item><p>To maximize a window along a side of the screen, grab the window’s
       titlebar and drag it to the left or right side of the screen.</p></item>
      <item><p>Όταν επισημανθεί η μισή οθόνη, ελευθερώστε το παράθυρο για να το μεγιστοποιήσετε κατά μήκος της επιλεγμένης πλευράς της οθόνης.</p></item>
      <item><p>Για μεγιστοποίηση δύο παραθύρων πλάι-πλάι, πάρτε τη γραμμή τίτλου του δεύτερου παραθύρου και σύρετε την στην αντίθετη πλευρά της οθόνης.</p></item>
       <item><p>Όταν επισημανθεί η μισή οθόνη, ελευθερώστε το παράθυρο για να το μεγιστοποιήσετε κατά μήκος της αντίθετης πλευράς της οθόνης.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-maximize-keyboard">
    <title>Μεγιστοποίηση και απομεγιστοποίηση παραθύρων χρησιμοποιώντας το πληκτρολόγιο</title>
    
    <steps>
      <item><p>Για μεγιστοποίηση ενός παραθύρου χρησιμοποιώντας το πληκτρολόγιο, κρατήστε πατημένο το πλήκτρο <key href="help:gnome-help/keyboard-key-super">Λογότυπο</key> και πατήστε <key>↑</key>.</p></item>
      <item><p>Για απομεγιστοποίηση ενός παραθύρου χρησιμοποιώντας το πληκτρολόγιο, κρατήστε πατημένο το πλήκτρο <key href="help:gnome-help/keyboard-key-super">Λογότυπο</key> και πατήστε <key>↓</key>.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-tile-keyboard">
    <title>Παράθυρα παράθεσης χρησιμοποιώντας το πληκτρολόγιο</title>
    
    <steps>
      <item><p>Για μεγιστοποίηση ενός παραθύρου κατά μήκος της δεξιάς πλευράς της οθόνης, κρατήστε πατημένο το πλήκτρο <key href="help:gnome-help/keyboard-key-super">Λογότυπο</key> και πατήστε <key>→</key>.</p></item>
      <item><p>Για μεγιστοποίηση ενός παραθύρου κατά μήκος της αριστερής πλευράς της οθόνης, κρατήστε πατημένο το πλήκτρο <key href="help:gnome-help/keyboard-key-super">Λογότυπο</key> και πατήστε <key>←</key>.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-workspaces-keyboard">
    <title>Εναλλαγή χώρων εργασίας χρησιμοποιώντας το πληκτρολόγιο</title>
    
    <steps>
    
    <item><p>Για μετακίνηση σε έναν χώρο εργασίας που είναι κάτω από τον τρέχοντα χώρο εργασίας, πατήστε το πλήκτρο <keyseq><key href="help:gnome-help/keyboard-key-super">Λογότυπο</key><key>Page Down</key></keyseq>.</p></item>
    <item><p>Για μετακίνηση σε έναν χώρο εργασίας που είναι πάνω από τον τρέχοντα χώρο εργασίας, πατήστε το πλήκτρο <keyseq><key href="help:gnome-help/keyboard-key-super">Λογότυπο</key><key>Page Up</key></keyseq>.</p></item>

    </steps>
    
  </section>

</page>
